# Práctico 3 - Manipulacion de imagenes

[Manipulacion de Imagenes](https://gitlab.com/vision-por-computadora615916/practico-4-manipulacion-de-imagenes/-/blob/main/Manipulacion_Imagenes.py?ref_type=heads) este script en Python utiliza OpenCV para permitir al usuario dibujar un rectángulo sobre una imagen con el ratón, y luego recortar y guardar la sección seleccionada en un nuevo archivo `recorte.png`.

## Funcionalidad:

- Configuración Inicial: Importa las bibliotecas necesarias y define variables globales para el seguimiento del estado del dibujo y las coordenadas del rectángulo.

- Definición de Callback: Define la función draw_circle que maneja los eventos del ratón para iniciar, finalizar y dibujar el rectángulo.

- Carga de Imagen: Carga una imagen llamada `hoja.png` para el procesamiento.

- Bucle Principal: En un bucle, muestra la imagen y escucha las teclas:
        q: Salir del programa.
        g: Guardar el recorte del rectángulo en recorte.png.
        r: Reiniciar la imagen para empezar de nuevo.






